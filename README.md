
[![MIT License][license-shield]][license-url]




<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Wallcycle</h3>

  <p align="center">
    A simple, opensource, folder based, wallpaper switcher for Android
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

Sometimes, one wishes that the wallpaper on the Android home and lock screens would auto switch at a regular interval.  Wallcycle provides this automation

How wallcycle works:
* wallcycle selects an image file from a subfolder of an Android device's download folder
* the user can put as many image files as desired in the folder
* "albums" can also be created in the wallpaper folder by creating subfolders
* the user can select one or several of the available "albums" for wallcycle to choose the next wallpaper from
* wallcycle will ignore subfolders that start with a period (".").  This allows the user to synch the wallpaper folder (and all subfolders) with other devices using synchthing (wallcycle will ignore the folders that synchthing creates)

Wallcycle is intended to be a simple wallpaper switcher that runs entirely locally on the device.  
Wallcycle does not:
* have network or internet access
* track or collect any data
* display any ads



<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With
* [![Kotlin][kotlin.com]][Kotlin-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>




<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

