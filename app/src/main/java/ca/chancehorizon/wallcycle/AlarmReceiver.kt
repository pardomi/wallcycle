package ca.chancehorizon.wallcycle

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast



class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Toast.makeText(context, "from AlarmReceiver", Toast.LENGTH_SHORT).show()

        val wallcyclePrefs = context?.getSharedPreferences("ca.chancehorizon.wallcycle", 0)

        // retrieve the last frequency set by the user and display it in the spinner (default to daily)
        val lastScreensToSwitch = wallcyclePrefs!!.getInt("prefScreenToSwitch", 2)

        // change the wallpaper for either one screen (lock or home) or both screen (with same wallpaper)
        if (lastScreensToSwitch < 3) {
            setWallpaper(context, lastScreensToSwitch)
        }
        // change the wallpaper for the lock and home screens (individually)
        else {
            setWallpaper(context, 0)
            setWallpaper(context, 1)
        }

        // update shared preferences to the user selected frequency
        val edit: SharedPreferences.Editor = wallcyclePrefs.edit()
        edit.putLong("lastAlarm", System.currentTimeMillis())
        edit.apply()
    }
}

