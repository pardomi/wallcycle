package ca.chancehorizon.wallcycle

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.os.Build.VERSION.SDK_INT
import android.provider.Settings
import android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    private var appContext: Context? = null

    var autoWallpaperAlarmActive = false
    var alarmFrequency = AlarmManager.INTERVAL_DAY
    val STORAGE_PERMISSION_CODE = 101


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appContext = getApplicationContext()

        setContentView(R.layout.activity_main)

        // check for storage access permission (cannot read wallpaper files without it)
        setupPermissions()

        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)

        autoWallpaperAlarmActive = wallcyclePrefs.getBoolean("alarmRunning", false)

        toggleAlarmSwitch.isChecked = autoWallpaperAlarmActive

        // retrieve the user set screens to switch the wallpaper for
        val lastScreensToSwitch = wallcyclePrefs.getInt("prefScreenToSwitch", 2)

        // show some details on status of switching wallpapers (active? which file?  when last change?)
        setStatusText()

        // show reduced size previews of currently selected wallpapers
        setWallpaperPreviews(lastScreensToSwitch)

        // set up the drop down list of wallpaper change frequencies
        val alarmFreqAdapter = ArrayAdapter.createFromResource(
            this,
            R.array.alarm_times,
            android.R.layout.simple_spinner_item
        )

        alarmFreqAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        alarmTimeSpinner.setAdapter(alarmFreqAdapter)

        val lastFrequency = wallcyclePrefs.getInt("prefAlarmFrequency", 1)
        alarmTimeSpinner.setSelection(lastFrequency)

        // react to the user selecting the frequency for switching wallpaper
        alarmTimeSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                // Do what you want
                val userSelection = alarmTimeSpinner.selectedItemPosition

                when (userSelection) {
                    0 -> alarmFrequency = AlarmManager.INTERVAL_HOUR / 30
                    1 -> alarmFrequency = AlarmManager.INTERVAL_HOUR
                    2 -> alarmFrequency = AlarmManager.INTERVAL_HALF_DAY / 2
                    3 -> alarmFrequency = AlarmManager.INTERVAL_HALF_DAY
                    4 -> alarmFrequency = AlarmManager.INTERVAL_DAY
                    5 -> alarmFrequency = AlarmManager.INTERVAL_DAY * 7
                    else -> {
                        alarmFrequency = AlarmManager.INTERVAL_DAY
                    }
                }

                // check if the user selection is different from the previous selected frequency
                if (lastFrequency != userSelection && autoWallpaperAlarmActive) {
                    // restart the alarm to newly user selected frequency
                    stopAutoWallpaperAlarm()
                    // tell the start alarm function that a new wallpaper change frequency has been set
                    startAutoWallpaperAlarm(true)
                }
                // update shared preferences to the user selected frequency
                val edit: SharedPreferences.Editor = wallcyclePrefs.edit()
                edit.putInt("prefAlarmFrequency", userSelection)
                edit.apply()

            }
        }

        // set up the drop down list of wallpaper change frequencies
        val screensToSwitchAdapter = ArrayAdapter.createFromResource(
            this,
            R.array.screensToSwitch,
            android.R.layout.simple_spinner_item
        )

        screensToSwitchAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        screensSpinner.setAdapter(screensToSwitchAdapter)

        screensSpinner.setSelection(lastScreensToSwitch)

        screensSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val userSelection = screensSpinner.selectedItemPosition

                // update shared preferences to the user selected frequency
                val edit: SharedPreferences.Editor = wallcyclePrefs.edit()
                edit.putInt("prefScreenToSwitch", userSelection)
                edit.apply()

                setWallpaperPreviews(userSelection)
            }
        }

        // react to user clicking on default album checkbox
        //  (save a list of seleccted albums to preferences file)
        defaultAlbum.setOnClickListener {
            saveSelectedFolders()
        }


        // Initialize the variable in before activity creation is complete.
        val storagePermissionResultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    // Permission granted. Now resume your workflow.
                    setupPermissions()
                }
            }
        }

        // react to user topping the "switch wallpaper now" button
        changeWallpaperButton.setOnClickListener {

            if (lastScreensToSwitch < 3) {
                setWallpaper(this, lastScreensToSwitch, "manual")
                setWallpaperPreviews(lastScreensToSwitch)
            }
            else {
                setWallpaper(this, 0, "manual")
                setWallpaperPreviews(0)
                setWallpaper(this, 1, "manual")
                setWallpaperPreviews(1)
            }
        }

        // react to user tapping on the set alarm switch (starting or stopping)
        toggleAlarmSwitch.setOnClickListener {

            if (autoWallpaperAlarmActive) {
                stopAutoWallpaperAlarm()
            }
            else {
                startAutoWallpaperAlarm()
            }
        }

        // react to user tapping on the storage permission needed warning
        textPermissionNeeded.setOnClickListener {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                //request for the permission
                try {
                    val intent = Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                    intent.data =
                        Uri.parse(String.format("package:%s", applicationContext.packageName))
                    storagePermissionResultLauncher.launch(intent)
                } catch (e: java.lang.Exception) {
                    val intent = Intent()
                    intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
                    storagePermissionResultLauncher.launch(intent)
                }
            }
            else {
                //Android is below 11(R)
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE
                )
            }
        }

        // react to user tapping on the battery optimization warning
        textBatteryOptimization.setOnClickListener {
            setBatteryOptimization(this)
        }

        checkBatteryOptimization()

        // show the list of available folders (wallpaper albums) that the user can choose
        listFolders()

        readSelectedFolders()
    }



    override fun onResume() {
        super.onResume()

        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        // retrieve the user set screens to switch the wallpaper for
        val lastScreensToSwitch = wallcyclePrefs.getInt("prefScreenToSwitch", 2)

        setStatusText()

        setWallpaperPreviews(lastScreensToSwitch)

        checkBatteryOptimization()
    }



    // display some details on the status of the most recent wallpaper switch
    fun setStatusText() {
        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val lastChange = dateTimeFromMillis(wallcyclePrefs!!.getLong("lastChange", 0))
        val lastImage = wallcyclePrefs.getString("lastImage", "N/A")
        val lastTrigger = wallcyclePrefs.getString("lastTrigger", "N/A")
        val lastAttempt = dateTimeFromMillis(wallcyclePrefs.getLong("lastAttempt", 0))
        val nextAttempt = dateTimeFromMillis(wallcyclePrefs.getLong("lastAttempt", 0) + alarmFrequency)

        autoWallpaperAlarmActive = wallcyclePrefs.getBoolean("alarmRunning", false)

        val lastWallpaperText = lastImage + " set on: " + lastChange + " by: " + lastTrigger +
                "\nLast Auto change: " + lastAttempt +
                "\nAuto change on: " + autoWallpaperAlarmActive.toString() +
                "\nNext Change: " + nextAttempt
        textLastWallpaper.setText(lastWallpaperText)
    }



    // create reduced size previews to be shown on main screen
    private fun setWallpaperPreviews(theScreen : Int) {
        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val lastImage = wallcyclePrefs.getString("lastImage", "N/A")
        val lastImageFolder = wallcyclePrefs.getString("lastImageFolder", "N/A")

        currentWallpaperImage.setImageBitmap(createImagePreview(lastImage, lastImageFolder))

        // show both home screen and lock screen wallpapers
        if(theScreen == 3) {
            val lastLockImage = wallcyclePrefs.getString("lastLockImage", "N/A")
            val lastLockImageFolder = wallcyclePrefs.getString("lastLockImageFolder", "N/A")
            currentLockWallpaperImage.setImageBitmap(createImagePreview(lastLockImage, lastLockImageFolder))
            currentLockWallpaperImage.setVisibility(View.VISIBLE)
        }
        // do not show lock screen wallpaper
        else {
            currentLockWallpaperImage.setVisibility(View.GONE)
        }
    }



    private fun createImagePreview(lastImage : String?, lastFolder : String? = "") : Bitmap {
        // get current wallpaper details
        var childFolder = "wallpaper"
        if (lastFolder != ""){
            childFolder = childFolder + System.getProperty("file.separator") + lastFolder
        }
        val theFolder = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), childFolder)
        val wallpaperFile = File(theFolder.path + System.getProperty("file.separator") + lastImage)

        var wallpaperBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)

        // show a preview of the current wallpaper
        if (wallpaperFile.exists()) {
            try {
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeFile(wallpaperFile.path, options)
                val theWidth = options.outWidth
                val theHeight = options.outHeight
                val aspectRatio = theWidth.toFloat() / theHeight.toFloat()
                val width = 1080
                val height = Math.round((width / aspectRatio).toDouble()).toInt()

                val theBitmap = BitmapFactory.decodeFile(wallpaperFile.path)
                wallpaperBitmap = Bitmap.createScaledBitmap(theBitmap, width, height, false)
            } catch (e: Exception) {
                textLastWallpaper.setText("oops!")
            } catch (e: java.io.FileNotFoundException) {
                textLastWallpaper.setText("oops!")
            }
        }

        return wallpaperBitmap
    }



    private fun setupPermissions() {

        if (checkPermission()) {
            textPermissionNeededTitle.visibility = View.GONE
            textPermissionNeeded.visibility = View.GONE
        }
        else {
            textPermissionNeededTitle.visibility = View.VISIBLE
            textPermissionNeeded.visibility = View.VISIBLE
        }
    }



    private fun checkPermission(): Boolean{
        return if (SDK_INT >= Build.VERSION_CODES.R){
            //Android is 11(R) or above
            Environment.isExternalStorageManager()
        }
        else {
            //Android is below 11(R)
            val read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            read == PackageManager.PERMISSION_GRANTED
        }
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == STORAGE_PERMISSION_CODE){
            if (grantResults.isNotEmpty()){
                //check each permission if granted or not
                val read = grantResults[0] == PackageManager.PERMISSION_GRANTED

                setupPermissions()
            }
        }
    }



    // check if battery optimization has been set for wallcycle app
    private fun checkBatteryOptimization() {
        val powerManager =
            this.getSystemService(POWER_SERVICE) as PowerManager

        if (powerManager.isIgnoringBatteryOptimizations(packageName)) {
            // battery optimization is OFF for wallcycle
            textBatteryOptimizationTitle.visibility = View.GONE
            textBatteryOptimization.visibility = View.GONE
        }
        else {
            // show that battery optimization is ON for wallcycle
            textBatteryOptimizationTitle.visibility = View.VISIBLE
            textBatteryOptimization.visibility = View.VISIBLE
        }
    }



    // let user set Android's battery optimization setting for Paseo
    private fun setBatteryOptimization(mContext: Context) {

        val packageName = mContext.packageName

        val batterySettingIntent = Intent()

        batterySettingIntent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS

        batterySettingIntent.data = Uri.parse("package:$packageName")
        batterySettingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        try {
            mContext.startActivity(batterySettingIntent)
        }
        catch (e: Error) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
        finally {
            checkBatteryOptimization()
        }

    }



    // start the alarm that switches the wallpaper at a user selected frequency
    fun startAutoWallpaperAlarm(newFrequency : Boolean = false) {

        // only start the wallpaper change alarm if the alarm in not active
        //   OR
        //  the user has changed the frequency for changing wallpapers
        if (!autoWallpaperAlarmActive || newFrequency) {

            // set up the alarm to reset steps after midnight
            registerMyAlarmBroadcast()
        }

        Toast.makeText(this, "Alarm is ON", Toast.LENGTH_SHORT).show()
        autoWallpaperAlarmActive = true

        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val edit: SharedPreferences.Editor = wallcyclePrefs!!.edit()

        edit.putBoolean("alarmRunning", true)
        edit.putInt("alarmFreqMinutes", (alarmFrequency / 60 / 1000).toInt())
        edit.apply()

        onResume()
    }



    // stop the alarm from running
    private fun stopAutoWallpaperAlarm() {

        val intent = Intent(this, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 1, intent,
            PendingIntent.FLAG_NO_CREATE or PendingIntent.FLAG_IMMUTABLE)

        if (pendingIntent != null) {
            val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            pendingIntent.cancel()
        }

        Toast.makeText(this, "Alarm is OFF", Toast.LENGTH_SHORT).show()

        autoWallpaperAlarmActive = false

        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val edit: SharedPreferences.Editor = wallcyclePrefs!!.edit()

        edit.putBoolean("alarmRunning", false)
        edit.apply()
    }



    // set up the alarm to switch wallpaper based on alarm repeating frequency
    private fun registerMyAlarmBroadcast() {

        val intent = Intent(this, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 1, intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
        val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(
            AlarmManager.ELAPSED_REALTIME_WAKEUP,
            SystemClock.elapsedRealtime() + 10000,
            alarmFrequency,
            pendingIntent
        )
    }



    // check if the alarm is already active
    fun isWallcycleAlarmActive () : Boolean {

        val alarmUp = PendingIntent.getBroadcast(
            this.getApplicationContext(), 0,
            Intent("ca.chancehorizon.wallcycle"),
            PendingIntent.FLAG_NO_CREATE or PendingIntent.FLAG_IMMUTABLE
        ) != null

        return alarmUp
    }



    // list wallpaper subfolders
    private fun listFolders() {

        // get the default wallpapers folder
        val theFolder = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "wallpaper")

        // get the listing of the wallpapers folder (to look for subfolders ("albums")
        val files = theFolder.listFiles()

        // use the location on the screen of the default "album" as the top of the list of "albums"
        var topView = findViewById(R.id.defaultAlbum) as View

        // loop through the default folder listing to look for subfolders and add them to the list
        //  of "albums"
        if (files != null) {
            for (theFile in files.indices) {

                // only list folders (do not include folders that start with a period ("."), like syncthing floders)
                if (files.get(theFile).isDirectory && files.get(theFile).name.get(0).toString() != ".") {

                    // create the checkbox for the newly found "album"
                    val layout = findViewById(R.id.cardConstraint) as ConstraintLayout
                    val foldersChecks = CheckBox(this)
                    foldersChecks.layoutParams =
                        ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                        )
                    foldersChecks.text = files.get(theFile).name
                    foldersChecks.setId(View.generateViewId())
                    layout.addView(foldersChecks)
                    val set = ConstraintSet()
                    set.clone(layout)
                    set.connect(foldersChecks.getId(), ConstraintSet.START, layout.getId(), ConstraintSet.START,0)
                    set.connect(foldersChecks.getId(), ConstraintSet.TOP, topView.getId(), ConstraintSet.BOTTOM,0)
                    set.applyTo(layout)

                    // set the action when user checks or unchecks the newly created checkbox
                    //  (reset the selected "albums" in the preferences file
                    foldersChecks.setOnClickListener {
                        saveSelectedFolders()
                    }

                    // set the location for new "albums" to the one just added
                    topView = foldersChecks
                }
            }
        }
    }



    // store the user set of selected subfolders
    private fun saveSelectedFolders() {
        val theFolders = ArrayList<String?>()

        // loop through views looking for checkboxes (each available wallpaper folder)
        val layout = findViewById(R.id.cardConstraint) as ConstraintLayout
        for (i in 0 until layout.getChildCount()) {
            val child: View = layout.getChildAt(i)

            if (child is CheckBox) {
                // if the checkbox is checked, save the name of the checkbox to the JSON array
                if (child.isChecked) {
                    theFolders.add(child.text.toString())
                }
            }
        }

        if (theFolders.size < 1) {
            theFolders.add("")
        }

        val jsArray = JSONArray(theFolders)

        // update shared preferences to the user selected folders (only selected folders are saved in the shared preferences)
        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val edit: SharedPreferences.Editor = wallcyclePrefs.edit()
        edit.putString("theFolders", jsArray.toString())
        edit.apply()
    }



    // retrieve the user selected folders and set the checkboxes for each
    private fun readSelectedFolders() {
        // retrieve the user selected folders (only selected folders are saved in the shared preferences)
        val wallcyclePrefs = this.getSharedPreferences("ca.chancehorizon.wallcycle", 0)

        val theFolders = wallcyclePrefs.getString("theFolders", "")

        // loop through views looking for checkboxes (each available wallpaper folder)
        val layout = findViewById(R.id.cardConstraint) as ConstraintLayout
        for (i in 0 until layout.getChildCount()) {
            val child: View = layout.getChildAt(i)

            if (child is CheckBox) {
                // if the checkbox is checked, save the name of the checkbox to the JSON array
                if (theFolders != null) {
                    child.isChecked = theFolders.contains(child.text)
                }
            }
        }
    }
}





//==========================
//==== HELPER FUNCTIONS ====

// choose a file "randomly" from the wallpapers folder and apply it to the home and lock screens
fun setWallpaper(theContext : Context, theScreen : Int = 0, trigger : String = "alarm") {

    val wallcyclePrefs = theContext.getSharedPreferences("ca.chancehorizon.wallcycle", 0)

    // update shared preferences
    val edit: SharedPreferences.Editor = wallcyclePrefs!!.edit()

    // make sure the wallpaper folder exists
    val wallpaperFolder = createWallpaperFolder()
    val theFolders:List<String>

    // get list all files in wallpapers folder
    var foldersString = wallcyclePrefs.getString("theFolders", "")
    if (foldersString != null && foldersString != "[\"\"]") {
        foldersString = foldersString.replace("[", "")
        foldersString = foldersString.replace("]", "")
        foldersString = foldersString.replace("\"", "")
        theFolders = foldersString.split(",")
    }
    else {
        theFolders = emptyList()
    }

    var files : Array<File>
    files = emptyArray()

    if (theFolders != null || theFolders.isEmpty()) {
        for (i in 0 until theFolders.size) {
            val folder = theFolders[i]
            if (folder == "Default Album") {
                files = wallpaperFolder.listFiles()
            }
            else {
                val theFolder = File(wallpaperFolder, theFolders[i])

                if (theFolder.exists()) {
                    files = files + theFolder.listFiles()
                }
            }
        }
    }
    // no "albums" are selected - do not change wallpaper
    else {
        // *** put toast here to tell user to select an album
        return
    }

    var theWallpaperFilename = ""
    var theWallpaperFolder = ""
    val screenFlag: Int

    when (theScreen) {
        0 -> screenFlag = WallpaperManager.FLAG_SYSTEM
        1 -> screenFlag = WallpaperManager.FLAG_LOCK
        else -> screenFlag = WallpaperManager.FLAG_SYSTEM or WallpaperManager.FLAG_LOCK
    }

    // make sure there are files in folder first
    if (files != null && files.isNotEmpty() && files.lastIndex > 0) {

        // select a file from the folder randomly
        val numberOfFiles = files.lastIndex
        val randomGenerator = Random(System.currentTimeMillis())
        val theFileNum = randomGenerator.nextInt(numberOfFiles)

        val theWallpaperFile = files[theFileNum]
        theWallpaperFilename = theWallpaperFile.name
        theWallpaperFolder = theWallpaperFile.parentFile.getName()
        if (theWallpaperFolder == "wallpaper") {
            theWallpaperFolder = ""
        }

        // m
        if (theWallpaperFile.exists() && isImage(theWallpaperFile)) {
            val bmp = BitmapFactory.decodeFile(theWallpaperFile.path)
            val wpManager = WallpaperManager.getInstance(theContext)
            wpManager.setBitmap(bmp, null, false, screenFlag)
        }
        else {
            edit.putString("lastSuccess", "No File")
        }
    }
    else {
        edit.putString("lastSuccess", "No Files Found")
    }

    edit.putLong("lastChange", System.currentTimeMillis())
    if (theScreen == 1) {
        edit.putString("lastLockImage", theWallpaperFilename)
        edit.putString("lastLockImageFolder", theWallpaperFolder)
    }
    else {
        edit.putString("lastImage", theWallpaperFilename)
        edit.putString("lastImageFolder", theWallpaperFolder)
    }
    edit.putString("lastTrigger", trigger)

    if (trigger == "alarm") {
        edit.putLong("lastAttempt", System.currentTimeMillis())
    }

    edit.apply()
}



// check if wallpaper folder exists, if not create it
private fun createWallpaperFolder() : File {
    val theFolder = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "wallpaper")

    if (!theFolder.exists()) {
        try {
            theFolder.mkdir()
        }
        catch (e: IOException) {
        }
    }

    return theFolder
}



// check if file is an image file
private fun isImage(file: File?): Boolean {
    if (file == null || !file.exists()) {
        return false
    }
    val options = BitmapFactory.Options()
    options.inJustDecodeBounds = true
    BitmapFactory.decodeFile(file.path, options)
    return options.outWidth != -1 && options.outHeight != -1
}



// convert millisecond to human readable date string
private fun dateTimeFromMillis(dateInMillis : Long) : String {

    val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
    val dateString = formatter.format( Date(dateInMillis))

    return dateString
}
