package ca.chancehorizon.wallcycle

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast



// when an wallswitch is killed, restart the wallpaper switching alarm
class Restarter : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {

        Toast.makeText(context, "REEEESTART.", Toast.LENGTH_SHORT).show()

        val mainActivity  = MainActivity()

        // update shared preferences to the user selected frequency
        val wallcyclePrefs = context.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val edit: SharedPreferences.Editor = wallcyclePrefs.edit()
        edit.putLong("restart", System.currentTimeMillis())
        edit.apply()

        mainActivity.startAutoWallpaperAlarm()
    }
}
