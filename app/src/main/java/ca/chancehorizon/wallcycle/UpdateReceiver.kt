package ca.chancehorizon.wallcycle

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast



// when an update to wallcycle is installed, restart the wallpaper switching alarm
class UpdateReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null) {
            return
        }

        Toast.makeText(context, "UUUPPDATEEE.", Toast.LENGTH_SHORT).show()

        val mainActivity = MainActivity()

        // update shared preferences to the user selected frequency
        val wallcyclePrefs = context.getSharedPreferences("ca.chancehorizon.wallcycle", 0)
        val edit: SharedPreferences.Editor = wallcyclePrefs.edit()
        edit.putLong("update", System.currentTimeMillis())
        edit.apply()


        mainActivity.startAutoWallpaperAlarm()
    }
}
