package ca.chancehorizon.wallcycle

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent


// when the device is rebooted, restart the wallpaper switching alarm
class BootCompleteReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        if (intent?.action == "android.intent.action.BOOT_COMPLETED") {

            val mainActivity  = MainActivity()
            mainActivity.startAutoWallpaperAlarm()
        }
    }
}
